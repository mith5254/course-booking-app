import { useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

    const navigate = useNavigate();

    const [ firstname, setFirstname] = useState("");
    const [ lastname, setLastname] = useState("");
    const [ mobileNo, setMobileNo ] = useState("")
    const [ email, setEmail ] = useState(""); 
    const [ password1, setPassword1 ] = useState(""); 
    const [ password2, setPassword2 ] = useState("");

    const { user, setUser } = useContext(UserContext);

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    function registerUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                email : email
            })
        }).then(res => res.json()).then(userExist => {
            (!userExist) ?
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method : "POST",
                    headers : {
                        "Content-Type" : "application/json"
                    },
                    body : JSON.stringify({
                        firstName : firstname,
                        lastName : lastname,
                        email : email,
                        mobileNo : mobileNo,
                        password : password1
                    })
                }).then(res => res.json()).then(success => {
                    console.log(success);
                    if(success) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Registration Successful',
                            text: 'Welcome to Zuitt!'
                        })

                        navigate("/login");
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Something went wrong',
                            text: 'Something went wrong during User Registration. Please try again later.'
                        });
                    }
                })
            :
             Swal.fire({
                icon: 'error',
                title: 'Duplicate email found',
                text: 'Please provide a different email.'
              });
        });

    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== "" && password1 !== "" && password2 !== "" && firstname !== "" && lastname !== "") && (password1 === password2) && (mobileNo.length === 11)){
            setIsActive(true);
        }else{
            setIsActive(false);
        };
    }, [firstname, lastname, mobileNo, email, password1, password2]);

    return(
        (user.id) ?
            <Navigate to="/courses" />
        :
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group controlId="firstname">
                    <Form.Label>Firstname</Form.Label>
                    <Form.Control
                        type = "text"
                        placeholder = "Enter Firstname"
                        value={ firstname }
                        onChange={e => setFirstname(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="lastname">
                    <Form.Label>Lastname</Form.Label>
                    <Form.Control
                        type = "text"
                        placeholder = "Enter Fastname"
                        value={ lastname }
                        onChange={e => setLastname(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="mobileNum">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control
                        type = "text"
                        placeholder = "Ex. 09123456890"
                        value={ mobileNo }
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="userEmail">
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type = "email"
                        placeholder = "Enter Email"
                        value={ email }
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">We'll never share your email with anyone</Form.Text>
                </Form.Group>

                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type = "password"
                        placeholder = "Enter a password"
                        value={ password1 }
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>

                
                <Form.Group controlId="password2">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        type = "password"
                        placeholder = "Confirm your password"
                        value={ password2 }
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>

    

                {
                    isActive ?
                        <Button variant="success" type="submit" id="submitBtn" className='mt-2'>Register</Button>
                        :
                        <Button variant="danger" type="submit" id="submitBtn" className='mt-2' disabled>Check Account Details</Button>
                }

            </Form>
    )
}

